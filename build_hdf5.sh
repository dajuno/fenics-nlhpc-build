#!/bin/bash
source env_build.sh $ARCH

HDF5_VERSION="1.8.17"

mkdir -p ${BUILD_DIR}/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc -P tar https://www.hdfgroup.org/ftp/HDF5/releases/hdf5-${HDF5_VERSION}/src/hdf5-${HDF5_VERSION}.tar.bz2 && \
   tar -xf tar/hdf5-${HDF5_VERSION}.tar.bz2 && \
   cd hdf5-${HDF5_VERSION} && \
   CC=mpiicc ./configure --enable-parallel --prefix=${PREFIX} && \
   make -j ${BUILD_THREADS} && \
   make install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
