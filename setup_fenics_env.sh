#!/bin/bash
source env_build.sh $ARCH

pew in fenics-${TAG} pew add ${PREFIX}/lib/python2.7/site-packages

mkdir -p ${PREFIX}/bin

# Copy fenics environment for intel modules to fenics-`tag`/bin
if [ -f "${PREFIX}/bin/env_fenics_run.sh" ]; then
    cp ${PREFIX}/bin/env_fenics_run.sh ${PREFIX}/bin/env_fenics_run.sh.bak
fi
if [ -f "${PREFIX}/bin/env_build.sh" ]; then
    cp ${PREFIX}/bin/env_build.sh ${PREFIX}/bin/env_build.sh.bak
fi
cp env_build.sh ${PREFIX}/bin

echo "# source file of the fenics-${TAG} environment
# source this file, then run  pew workon fenics-${TAG}

module load Lmod/6.5
source \${LMOD_PROFILE}
module load intel/2016b
# imkl module replaces fpath and breaks zsh
fpath=( /usr/share/zsh/site-functions /usr/share/zsh/5.0.2/functions \"\${fpath[@]}\" )
module load Python/2.7.12
module load numpy/1.11.0-Python-2.7.12
module load CMake/3.5.2
module load HDF5/1.8.17
module load Boost/1.61.0
module load Eigen/3.2.9
module load PCRE/8.39
module load zlib/1.2.8

export CC=icc
export CXX=icpc
export FC=ifort
export F77=ifort
export F90=ifort

export MPICC=mpiicc
export MPICXX=mpiicpc
export MPIF77=mpiifort
export MPIF90=mpiifort
export MPIEXEC=mpiexec

export PREFIX=\$HOME/dev/fenics-${TAG}

export PATH=\${PREFIX}/bin:\${PATH}
export LD_LIBRARY_PATH=\${PREFIX}/lib:\${LD_LIBRARY_PATH}
export PYTHONPATH=\${PREFIX}/lib/python2.7/site-packages:\${PYTHONPATH}
export MANPATH=\${PREFIX}/share/man:\${MANPATH}
export PKG_CONFIG_PATH=\${PREFIX}/lib/pkgconfig:\${PKG_CONFIG_PATH}

export PETSC_DIR=\${PREFIX}
export SLEPC_DIR=\${PREFIX}

# export INSTANT_SYSTEM_CALL_METHOD=\"COMMANDS\"      # this fixes subprocess.py error on cluster
export INSTANT_CACHE_DIR=\${PREFIX}/cache/instant
export INSTANT_ERROR_DIR=\${PREFIX}/cache/instant" \
    > ${PREFIX}/bin/env_fenics_run.sh

chmod +x ${PREFIX}/bin/env_fenics_run.sh



echo "
Load FEniCS ${TAG} environment with 
    source ${PREFIX}/bin/env_fenics_run.sh

Load python virtualenv with
    pew workon fenics-${TAG}"
