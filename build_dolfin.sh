#!/bin/bash
source env_build.sh $ARCH

FENICS_VERSION="2016.2.0"
export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

mkdir -p $BUILD_DIR/tar

cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/dolfin/downloads/dolfin-${FENICS_VERSION}.tar.gz && \
    tar -xf tar/dolfin-${FENICS_VERSION}.tar.gz && \
    cd dolfin-${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    pew in fenics-${TAG} cmake .. \
        -DCMAKE_INSTALL_PREFIX=${PREFIX} \
        -DCMAKE_BUILD_TYPE="Release" \
        -DCMAKE_CXX_COMPILER=mpiicpc \
        -DCMAKE_CXX_FLAGS="-O3 -xHost -ip -mkl -std=c++11" \
        -DCMAKE_C_COMPILER=mpiicc \
        -DCMAKE_C_FLAGS="-O3 -xHost -ip -mkl" \
        -DCMAKE_Fortran_COMPILER=mpiifort \
        -DCMAKE_VERBOSE_MAKEFILE=ON \
        -DDOLFIN_ENABLE_SPHINX=OFF \
        -DDOLFIN_ENABLE_UMFPACK=ON \
        -DDOLFIN_ENABLE_CHOLMOD=ON \
        -DEIGEN3_INCLUDE_DIR=${EIGEN_DIR}/include \
        -DZLIB_INCLUDE_DIR=${ZLIB_DIR}/include \
        -DZLIB_LIBRARY=${ZLIB_DIR}/lib/libz.so \
        -DHDF5_hdf5_LIBRARY_RELEASE=${HDF5_DIR}/lib/libhdf5.so \
        -DMPIEXEC_MAX_NUMPROCS=120 \
        -DDOLFIN_ENABLE_TRILINOS=OFF \
        -DDOLFIN_ENABLE_BENCHMARKS=ON \
        -DDOLFIN_ENABLE_VTK=OFF \
        -DDOLFIN_AUTO_DETECT_MPI=ON \
        -DMPI_CXX_COMPILER=${MPI_ROOT}/bin64/mpiicpc \
        -DMPI_C_COMPILER=${MPI_ROOT}/bin64/mpiicc \
    2>&1 | tee cmake.log && \
    pew in fenics-${TAG} make -j ${BUILD_THREADS} && \
    pew in fenics-${TAG} make install

        # -DHDF5_hdf5_LIBRARY_RELEASE=${HDF5_DIR}/lib/libhdf5.so \
        # -DCMAKE_Fortran_FLAGS="-O3 -xHost -ip -mkl" \
        # -DDOLFIN_ENABLE_PASTIX=OFF \
        # -DMPI_Fortran_COMPILER=${MPI_ROOT}/bin64/mpiifort \
        # -DDOLFIN_ENABLE_QT=OFF \
        # -DHDF5_C_COMPILER_EXECUTABLE=${HDF5_DIR}/bin/h5pcc \
        # -DBOOST_ROOT=${BOOST_ROOT} \
        # -DZLIB_LIBRARY=${ZLIB_DIR}/lib/libz.a \
        # -DZLIB_INCLUDE_DIR=${ZLIB_DIR}/include \

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
