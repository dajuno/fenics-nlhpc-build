#!/bin/bash
source env_build.sh $ARCH

VERSION="0.23.4"

mkdir -p ${BUILD_DIR}/tar
mkdir -p ${PREFIX}/lib/python2.7/site-packages/

cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar/ https://pypi.python.org/packages/source/C/Cython/Cython-${VERSION}.tar.gz && \
    tar -xzf tar/Cython-${VERSION}.tar.gz && \
    cd Cython-${VERSION} && \
    pew in fenics-${TAG} python setup.py install --prefix=${PREFIX}


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
