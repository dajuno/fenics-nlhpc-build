#!/bin/bash
source env_build.sh $ARCH

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc -P tar https://www.hdfgroup.org/ftp/lib-external/szip/2.1/src/szip-2.1.tar.gz && \
   tar -xf tar/szip-2.1.tar.gz && \
   cd szip-2.1 && \
   ./configure --prefix=${PREFIX} && \
   make && \
   make install



if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
