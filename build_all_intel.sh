#!/bin/bash

# exit on error
set -e

# FENICS version, only for build name (folder, virtual env)
FENICS_VERSION="2016.2.0"

export ARCH="intel2016" # intel, openmpi, mpich
                    #                      XXX: intel only, don't try!

export BUILD_THREADS=4
export TAG="${FENICS_VERSION}-${ARCH}b"      # 1.6-intel
export PREFIX=${HOME}/dev/fenics-${TAG}
export BUILD_DIR=${HOME}/dev/build/fenics-${TAG}
mkdir -p ${PREFIX}

export continue_on_key=false  # set this to true in order to wait after each module

echo "Installing FEniCS to ${PREFIX}"

./setup_virtualenv.sh  # if not, make sure virtualenv fenics-${tag} exists!!
./build_petsc.sh
./build_slepc.sh
# ./build_hdf5.sh
./build_swig.sh
./build_python_modules.sh
./build_fenics_pymodules.sh   # ffc fiat ufl uflacs instant
./build_dolfin.sh
./setup_fenics_env.sh

# run with $ ./build_all_intel.sh |& tee -a build.log
