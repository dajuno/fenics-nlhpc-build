#!/bin/bash
source env_build.sh $ARCH

OPENBLAS_VERSION="0.2.15"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc https://github.com/xianyi/OpenBLAS/archive/v${OPENBLAS_VERSION}.tar.gz -O openblas.tar.bz2 && \
   tar -xf tar/openblas.tar.bz2 && \
   cd OpenBLAS-${OPENBLAS_VERSION} && \
   make -j1 PREFIX=${PREFIX} USE_THREAD=0 NUM_THREADS=1 NO_PARALLEL_MAKE=1 && \
   make install PREFIX=${PREFIX}


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
