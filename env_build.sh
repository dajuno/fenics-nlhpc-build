# source this in your shell

# First decide if ARCH=gnu passed via args
# arg=$@
# if [ "$arg" = "mpich" ]; then
#     module load python/2.7.10

# elif [ "$arg" = "openmpi" ]; then
#     module load python/2.7.10

echo "loading modules" 
module load Lmod/6.5
source $LMOD_PROFILE
module load intel/2016b
# imkl module replaces fpath and breaks zsh
fpath=( /usr/share/zsh/site-functions /usr/share/zsh/5.0.2/functions "${fpath[@]}" )
module load Python/2.7.12
module load numpy/1.11.0-Python-2.7.12
module load CMake/3.5.2
module load HDF5/1.8.17
module load Boost/1.61.0
module load Eigen/3.2.9
module load PCRE/8.39
module load zlib/1.2.8

echo "setting intel compilers"
# set env variables for intel compilers and MKL (is this really necessary??)
# source ${INTEL_HOME}/bin/iccvars.sh intel64
# source ${MKLROOT}/bin/mklvars.sh intel64
export CC=icc
export CXX=icpc
export FC=ifort
export F77=ifort
export F90=ifort

export MPICC=mpiicc
export MPICXX=mpiicpc
export MPIF77=mpiifort
export MPIF90=mpiifort
export MPIEXEC=mpiexec
# print out module list for checking
module list

# $BOOST_ROOT set by module
export HDF5_DIR=${EBROOTHDF5}
export ZLIB_DIR=${EBROOTZLIB}
export EIGEN_DIR=${EBROOTEIGEN}


if [ ! -z "${PREFIX}" ]; then
    export PATH=${PREFIX}/bin:${PATH}
    export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
    export INCLUDE_PATH=${PREFIX}/include:${INCLUDE_PATH}
    export PYTHONPATH=${PREFIX}/lib/python2.7/site-packages:${PYTHONPATH}
fi
