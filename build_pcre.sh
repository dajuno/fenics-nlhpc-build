#!/bin/bash
source env_build.sh $ARCH

PCRE_VERSION="8.38"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc -P tar ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-${PCRE_VERSION}.tar.gz && \
   tar -xzf tar/pcre-${PCRE_VERSION}.tar.gz && \
   cd pcre-${PCRE_VERSION} && \
   ./configure \
       --disable-dependency-tracking \
       --enable-utf8 \
       --enable-pcre8 \
       --enable-pcre16 \
       --enable-pcre32 \
       --enable-unicode-properties \
       --enable-jit \
       --prefix=${PREFIX} && \
   make && \
   make install

# NOTE: configure options taken from hashstack yaml file


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
