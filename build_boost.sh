#!/bin/bash
source env_build.sh $ARCH

mkdir -p ${BUILD_DIR}/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://sourceforge.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.gz/download -O tar/boost.tar.gz && \
   tar -xf tar/boost.tar.gz && \
   cd boost_1_60_0 && \
   ./bootstrap.sh --prefix=$PREFIX --with-libraries=filesystem,program_options,system,thread,timer,iostreams && \
   ./b2 install


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
