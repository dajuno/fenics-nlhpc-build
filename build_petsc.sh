#!/bin/bash
source env_build.sh $ARCH

VERSION="3.7.4"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc -P tar/ http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${VERSION}.tar.gz && \
   tar -xzf tar/petsc-${VERSION}.tar.gz && \
   cd petsc-${VERSION} && \
   ./configure \
        --with-cc=${MPI_ROOT}/bin64/mpiicc \
        --with-cxx=${MPI_ROOT}/bin64/mpiicpc \
        --with-fc=${MPI_ROOT}/bin64/mpiifort \
        --with-shared-libraries \
        --with-debugging=0 \
        --with-c-support \
        --with-cxx-dialect=C++11 \
        --with-batch \
        --known-mpi-shared-libraries=1 \
        --COPTFLAGS="-O3 -xHost -ip" \
        --CXXOPTFLAGS="-O3 -xHost -ip" \
        --FOPTFLAGS="-O3 -xHost -ip" \
        --with-blas-lapack-dir=${MKLROOT}/lib/intel64 \
        --download-hypre \
        --download-mumps \
        --download-ml \
        --download-metis \
        --download-parmetis \
        --download-suitesparse \
        --download-ptscotch \
        --download-scalapack \
        --download-blacs \
        --download-superlu_dist \
        --prefix=${PREFIX} && \
    srun -n 1 conftest-arch-linux2-c-opt && \
    ./reconfigure-arch-linux2-c-opt.py && \
    make MAKE_NP=${BUILD_THREADS} && make install


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

