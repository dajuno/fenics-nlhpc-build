#!/bin/bash
source env_build.sh $ARCH

ZLIB_VERSION="1.2.8"
mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc -P tar http://zlib.net/zlib-1.2.8.tar.gz && \
   tar -xf tar/zlib-1.2.8.tar.gz && \
   cd zlib-1.2.8 && \
   ./configure --prefix=${PREFIX} && \
   make && \
   make install



if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
