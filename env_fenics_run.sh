# source file of the fenics-1.6-intel environment
# source this file, then run  pew workon fenics-1.6-intel

module load python/2.7.10 
module load hdf5/1.8.15 
module load boost/1.59
module load impi/5.1.1.109
module load intel/14.0.2

export CC=icc
export CXX=icpc
export FC=ifort
export F77=ifort
export F90=ifort

export MPICC=mpiicc
export MPICXX=mpiicpc
export MPIF77=mpiifort
export MPIF90=mpiifort
export MPIEXEC=mpiexec

export PREFIX=$HOME/dev/fenics-1.6-intel

export PATH=${PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${PREFIX}/lib/python2.7/site-packages:${PYTHONPATH}
export MANPATH=${PREFIX}/share/man:${MANPATH}
export PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig:${PKG_CONFIG_PATH}

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

export INSTANT_SYSTEM_CALL_METHOD="COMMANDS"
export INSTANT_CACHE_DIR=${PREFIX}/cache/instant
export INSTANT_ERROR_DIR=${PREFIX}/cache/instant
