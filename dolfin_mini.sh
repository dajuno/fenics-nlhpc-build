#!/bin/sh
cd ~/dev/build/dolfin/build_dir
source ~/dev/build/cluster_env.sh
# export HDF5_ROOT=/home/dnolte/dev/hdf5-1.8.16/
export HDF5_ROOT=/home/apps/lib/hdf5/1.8.15
export PETSC_DIR=/home/dnolte/dev/petsc
~/dev/cmake/bin/cmake .. -DBOOST_ROOT=/home/apps/lib/boost/1.59 \
    -DCMAKE_INSTALL_PREFIX=/home/dnolte/dev/dolfin \
    -DCMAKE_BUILD_TYPE="Release" \
    -DCMAKE_CXX_COMPILER=mpiicpc \
    -DCMAKE_CXX_FLAGS="-O3 -xHost -ip -mkl" \
    -DCMAKE_C_COMPILER=mpiicc \
    -DCMAKE_C_FLAGS="-O3 -xHost -ip -mkl" \
    -DCMAKE_Fortran_COMPILER=mpiifort \
    -DCMAKE_Fortran_FLAGS="-O3 -xHost -ip -mkl" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DHDF5_C_COMPILER_EXECUTABLE=/home/apps/lib/hdf5/1.8.15/bin/h5pcc \
    -DHDF5_hdf5_LIBRARY_RELEASE=/home/apps/lib/hdf5/1.8.15/lib/libhdf5.so \
    -DHDF5_z_LIBRARY_RELEASE=/home/apps/lib/zlib/1.2.8/lib/libz.so \
    -DPETSC_DIR=/home/dnolte/dev/petsc \
    -DDOLFIN_ENABLE_SPHINX=OFF \
    -DDOLFIN_ENABLE_UMFPACK=ON \
    -DDOLFIN_ENABLE_CHOLMOD=ON \
    -DSLEPC_DIR=/home/dnolte/dev/slepc \
    -DZLIB_LIBRARY=/home/apps/lib/zlib/1.2.8/lib/libz.a \
    -DZLIB_INCLUDE_DIR=/home/apps/lib/zlib/1.2.8/include \
    -DEIGEN3_INCLUDE_DIR=/home/apps/lib/eigen/3.2.4/include/eigen3 \
    -DMPIEXEC_MAX_NUMPROCS=120 \
    -DDOLFIN_ENABLE_PASTIX=OFF \
    -DDOLFIN_ENABLE_TRILINOS=OFF \
    -DDOLFIN_ENABLE_BENCHMARKS=ON \
    -DDOLFIN_ENABLE_VTK=OFF \
    -DDOLFIN_ENABLE_QT=OFF \
    -DDOLFIN_AUTO_DETECT_MPI=ON \
    -DMPI_CXX_COMPILER=/home/apps/intel/2016/impi/5.1.1.109/bin64/mpiicpc \
    -DMPI_C_COMPILER=/home/apps/intel/2016/impi/5.1.1.109/bin64/mpiicc \
    -DMPI_Fortran_COMPILER=/home/apps/intel/2016/impi/5.1.1.109/bin64/mpiifort \
2>&1 | tee cmake.log

    # -DSLEPC_INCLUDE_DIRS=/home/dnolte/dev/slepc/include \
    # -DSLEPC_LIBRARY=/home/dnolte/dev/slepc/lib/libslepc.so \

    # -DVTK_DIR=/home/dnolte/dev/vtk-5.10.1 \

# automatically recognized if PETSC_DIR is set:
    # -DPARMETIS_INCLUDE_DIRS=/home/dnolte/dev/petsc/include \
    # -DPARMETIS_LIBRARY=/home/dnolte/dev/petsc/lib/libparmetis.so \
    # -DMETIS_LIBRARY=/home/dnolte/dev/petsc/lib/libmetis.so \
    # -DSCOTCH_INCLUDE_DIRS=/home/dnolte/dev/petsc/include \
    # -DSCOTCH_LIBRARY=/home/dnolte/dev/petsc/lib/libscotch.a \

    # -DEIGEN3_INCLUDE_DIR=/home/dnolte/dev/eigen/include/eigen3 \
# use own hdf5
    # -DHDF5_LIBRARIES=/home/dnolte/dev/hdf5-1.8.16/lib/libhdf5.so \
    # -DHDF5_C_COMPILER_EXECUTABLE=/home/dnolte/dev/hdf5-1.8.16/bin/h5pcc \
    # -DHDF5_hdf5_LIBRARY_RELEASE=/home/dnolte/dev/hdf5-1.8.16/lib/libhdf5.so \

# use system HDF5    
    # -DHDF5_DIR=/home/apps/lib/hdf5/1.8.15 \
    # -DHDF5_LIBRARIES=/home/apps/lib/hdf5/1.8.15/lib/libhdf5.so \
    # -DHDF5_C_COMPILER_EXECUTABLE=/home/apps/lib/hdf5/1.8.15/bin/h5pcc \
    # -DHDF5_hdf5_LIBRARY_RELEASE=/home/apps/lib/hdf5/1.8.15/lib/libhdf5.so \
    # -DZLIB_LIBRARY=/usr/lib64/libz.so \
    # -DHDF5_z_LIBRARY_RELEASE=/home/apps/lib/zlib/1.2.8/lib/libz.so
