#!/bin/bash
source env_build.sh $ARCH

SWIG_VERSION="3.0.10"

mkdir -p ${BUILD_DIR}/tar

cd ${BUILD_DIR} && \
    wget --read-timeout=10 -nc -P tar http://downloads.sourceforge.net/swig/swig-${SWIG_VERSION}.tar.gz && \
    tar xf tar/swig-${SWIG_VERSION}.tar.gz && \
    cd swig-${SWIG_VERSION} && \
    ./configure --without-octave  --without-java --prefix=${PREFIX} && \
    make -j ${BUILD_THREADS}  && \
    make install

        # --with-pcre-prefix=${PREFIX} \

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
