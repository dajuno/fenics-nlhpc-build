#!/bin/bash
source env_build.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

pew in fenics-${TAG} pip install petsc4py slepc4py
pew in fenics-${TAG} pip install pyyaml sympy ipython enum
pew in fenics-${TAG} pip install -I decorator
pew in fenics-${TAG} pip install -U scipy

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
