# fenics-nlhpc-build #
### Scripts for building FEniCS on the NLHPC Cluster at the Universidad de Chile ###
_based on [fenics-gaia-cluster](https://bitbucket.org/unilucompmech/fenics-gaia-cluster) kindly provided by [Jack Hale](https://bitbucket.org/jackhale/)_

These scripts will automatically build the latest stable version of
[FEniCS](http://fenicsproject.org) with PETSc on the
NLHPC cluster `leftraru`, using Intel compilers, MKL and Intel MPI.

For information about the cluster and account requests, see [NLHPC](http://www.nlhpc.cl).

** TODO **

* works only with intel and impi. Test openmpi and mpich!
* use `easybuild` to make building PETSc and DOLFIN simpler

## Prerequisites ##
It is recommendable to install the python modules inside [virtualenvs](http://docs.python-guide.org/en/latest/dev/virtualenvs) to manage your FEniCS build. In particular, this is necessary if you need to install more than one build. 
Here we use [pew](https://github.com/berdario/pew) to manage the virtualenvs.

We use the updated cluster modules provided by `Lmod`, compiled with `intel 2016` compilers.
To load the `Lmod` system, execute
```shell
$ module load Lmod/6.5
$ source $LMOD_PROFILE
```
and load the `intel/2016b` package (alternatively, at this point you can resort to GCC) with
```shell
$ module load intel/2016b
```
All `intel` compiled modules are now available, see
```shell
$ module avail
```
Among this modules is `Python/2.7.12`. To install `pew` locally, you can do:
```shell
$ module load Lmod/6.5
$ source $LMOD_PROFILE
$ module load intel/2016b
$ module load Python/2.7.12
$ pip install pew --user          # or --prefix=<dir>, see note below
```
Or, if different python versions are to be used, for example:
```shell
$ pip install pew --prefix=$HOME/python2.7_intel2016b
```
A virtualenv `fencics-${TAG}` for your FEniCS build is automatically created by the build scripts.
The prefix directory has to be added to the path by the environment scripts (`env_build.sh`, `env_fenics_run.sh`).

Note: 

* It might be better _not_ to use the `$HOME/.local` folder for python if you want to try building with a non-intel build of python. Separating both is difficult and cannot be done with virtualenvs (pew). Apparently, separate builds of pip/setuptools/virtualenv etc are needed!
* Some tweaking is required to make `ipython` run inside the virtualenv, e.g., see [here](https://coderwall.com/p/xdox9a/running-ipython-cleanly-inside-a-virtualenv). One possibility is by redefining the ipython alias:
```shell
alias ipython="python -c 'import IPython; IPython.terminal.ipapp.launch_new_instance()' \
    --HistoryManager.hist_file=/tmp/ipyhist.sqlite"
```

## Compiling instructions ##

First clone this repository, for example to the location `$HOME/dev`.
```shell
$ mkdir -p $HOME/dev && cd $HOME/dev
$ git clone git@bitbucket.org:dajuno/fenics-nlhpc-build.git
```
Adapt the scripts to the desired configuration.
In `build_all_intel.sh`: 

* set `$ARCH` to `intel` for Intel MPI (default) or to `openmpi` for OpenMPI(Intel) (**NOTE: NOT IMPLEMENTED**) and a version tag. This will not specify the FEniCS version to be built but only a tag to set the install directories and the python virtualenv. Example: '2016.2.0', 'test2'.
Make sure the `$TAG` variable is unique. Otherwise existing FEniCS installations may be overwritten.
* specify build and install (destination) directories (`$PREFIX`)
* (un)comment the modules you (don't) want to build

Make sure the environments are correctly set in `env_build.sh` and `setup_env_fenics.env`. Also revise the individual build files.

The current default build (as of Jan, 2017) is set up to use optimized cluster modules

- CMake/3.5.2
- Python/2.7.12
- numpy/1.11.0-Python-2.7.12
- HDF5/1.8.17
- Boost/1.61.0
- Eigen/3.2.9
- PCRE/8.39    (needed?)
- zlib/1.2.8

and the Intel compiler set, Intel MPI and MKL (blas). See the `env_build.sh` file.

The other modules are compiled with aggressive optimization (-O3).

Building can be done on the login node. The script can also be submitted to a compute node via `srun`.
```shell
$ cd $HOME/dev/fenics-nlhpc-build
$ ./build_all_intel.sh |& tee build.log
```
Wait for the build to finish. The output of
the build will be stored in `build.log` as well as outputted to the screen.

## Running FEniCS on Leftraru ##
To activate a FEniCS build you need to source the environment file created by the build script and activate the virtualenv. The corresponding lines are printed after building is completed.
```shell
$ source <prefix>/bin/env_fenics_run.sh
$ pew workon fenics-<tag>
```
Now you can run python/ipython. `import dolfin` should work and submitting a job to scheduler by `srun -n 1 python poisson.py`.

These lines need to be included in the job script. Instead of invoking the virtualenv by `pew workon` beforehand, `srun pew in fenics-<tag> python poisson.py` can be used.

Example job script for `leftraru`:
```bash
#!/bin/bash                                             
#SBATCH --job-name=fenics-test                         
#SBATCH --partition=slims                               
#SBATCH -N 6                                   # compute on six nodes
#SBATCH --ntasks-per-node=4                    # assign 4 tasks to each node
#SBATCH --exclusive                            # no other tasks can run on the node
#SBATCH --output=6x4_%j.out                             
#SBATCH --error=6x4_%j.err                              
#SBATCH --mail-user=dnolte@dim.uchile.cl                
#SBATCH --mail-type=ALL                                 
                                                        
source $HOME/dev/fenics-1.6-intel/bin/env_fenics_run.sh 
srun pew in fenics-1.6-intel python ${cwd}/poisson.py
```