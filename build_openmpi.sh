#!/bin/bash
source env_build.sh $ARCH

OPENMPI_VERSION="1.10.2"

mkdir -p ${BUILD_DIR}/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://www.open-mpi.org/software/ompi/v1.10/downloads/openmpi-${OPENMPI_VERSION}.tar.gz -O tar/openmpi.tar.gz && \
   tar -xf openmpi.tar.gz && \
   cd openmpi-${OPENMPI_VERSION} && \
   ./configure --prefix=${PREFIX} --with-cma && \
   make && \
   make install


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
