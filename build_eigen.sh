#!/bin/bash
source env_build.sh $ARCH

VERSION="3.2.7"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://bitbucket.org/eigen/eigen/get/${VERSION}.tar.bz2 -O tar/eigen.tar.bz2 && \
   tar -xf tar/eigen.tar.bz2 && \
   cd eigen-eigen-b30b87236a1b && \
   mkdir -p build && \
   cd build && \
   cmake ../ -DCMAKE_INSTALL_PREFIX=${PREFIX} && \
   make install


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
