#!/bin/bash
source env_build.sh $ARCH

FENICS_VERSION="2016.2.0"

mkdir -p $BUILD_DIR/tar
# mkdir -p ${PREFIX}/lib/python2.7/site-packages/


# FIAT
cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/fiat/downloads/fiat-${FENICS_VERSION}.tar.gz && \
    tar -xf tar/fiat-${FENICS_VERSION}.tar.gz && \
    cd fiat-${FENICS_VERSION} && \
    pew in fenics-${TAG} python setup.py install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

# INSTANT
cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/instant/downloads/instant-${FENICS_VERSION}.tar.gz && \
    tar -xf tar/instant-${FENICS_VERSION}.tar.gz && \
    cd instant-${FENICS_VERSION} && \
    pew in fenics-${TAG} python setup.py install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

# UFL
cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/ufl/downloads/ufl-${FENICS_VERSION}.tar.gz && \
    tar -xf tar/ufl-${FENICS_VERSION}.tar.gz && \
    cd ufl-${FENICS_VERSION} && \
    pew in fenics-${TAG} python setup.py install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

# DIJITSO
cd $BUILD_DIR && \
   wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/dijitso/downloads/dijitso-${FENICS_VERSION}.tar.gz && \
   tar -xf tar/dijitso-${FENICS_VERSION}.tar.gz && \
   cd dijitso-${FENICS_VERSION} && \
   pew in fenics-${TAG} python setup.py install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

# FFC
cd $BUILD_DIR && \
    wget --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/ffc/downloads/ffc-${FENICS_VERSION}.tar.gz && \
    tar -xf tar/ffc-${FENICS_VERSION}.tar.gz && \
    cd ffc-${FENICS_VERSION} && \
    pew in fenics-${TAG} python setup.py install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

