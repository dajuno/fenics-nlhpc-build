// vim: syntax=markdown
# Install Notes -- FEniCS on leftraru@NLHPC

The FEniCS project 

Recommendation:
    - building is done in dev/build/<pkg>
    - compiled programs in dev/<pkg> (e.g. dev/petsc)

First, load cluster modules and source environment variables. 
Example: source cluster_env.sh

Modules used: 
  1) impi/5.0.3      3) intel/14.0.2    5) python/2.7.10
  2) boost/1.57      4) intel/15.0.1

We have to use the intel 14 compilers because of a licence checkout problem
with intel 2015. intel/15.0.1 is loaded by impi as a dependency. Make sure to
load intel/14.0.2 last. 
If `icc -v` takes several seconds, the wrong module is loaded. Check with
`module list` and `which icc` that the right version is used.

NOTE: For make, often you need to set the compiler via `export CC=icc` etc
(check cluster_env.sh).
NOTE: Standard MPI compilers are mpicc, mpifort, mpicpc. Make sure to use
mpiicc, mpiicpc, mpiifort instead to use the correct intel impi version.


## Cython
install cython locally
pip install cython --user


## PETSc
see https://www.mcs.anl.gov/petsc/documentation/installation.html

1. Download PETSc
git clone -b maint https://bitbucket.org/petsc/petsc petsc

2. Configure
  * load modules:
        source cluster_env.sh
  * run configure script:
        ./arch-nlhpc-intel.py

  * follow instructions

3. install petsc4py
export PETSC_DIR=<path>
pip install petsc4py --user


## SLEPc
see http://slepc.upv.es/documentation/instal.htm

export SLEPC_DIR=<path>
export PETSC_DIR=<path>
export PETSC_ARCH=      
set empty if petsc was installed with --prefix, i.e., there is no $PETSC_DIR/$PETSC_ARCH folder

./configure
make
make install
make test

## CMAKE
download, then install by:

./bootstrap --prefix=$HOME/dev/cmake
make 
make install

## SZIP (HDF5 dependency)
***NOTE*** DON'T BUILD HDF5 (see below)
see https://software.intel.com/en-us/articles/performance-tools-for-software-developers-building-hdf5-with-intel-compilers

export CC=icc
export CXX=icpc
export FC=ifort
export CFLAGS='-O3 -xHost -ip'
export CXXFLAGS='-O3 -xHost -ip'
export FCFLAGS='-O3 -xHost -ip'
tar -zxvf szip-2.1.tar.gz
cd szip-2.1
./configure --prefix=$HOME/dev/szip-2.1
make
make check
make install

## ZLIB (HDF5 dep)
***NOTE*** DON'T BUILD HDF5 (see below)
export CC=icc
export CFLAGS='-O3 -xHost -ip'
tar -zxvf zlib-1.2.7.tar.gz
cd zlib-1.2.7 
./configure --prefix=$HOME/dev/zlib-1.2.7 
make
make check
make install

## HDF5

***NOTE***: HDF5/1.8.15 AVAILABLE ON THE CLUSTER WORKS WITH SYSTEM H5PY!
BUILDING OWN HDF5 NOT NECESSARY ANYMORE
 

see https://www.hdfgroup.org/ftp/HDF5/current/src/unpacked/release_docs/INSTALL_parallel

optimization detected automatically!

compile on compute nodes:
    * interactive slurm session: 
        srun -I --pty /bin/bash
    * load modules (cluster_env.sh)
    * configure HDF5 (according to hashstack hdf5.yaml and above):

export CC=$MPICC
export F9X=$MPIF90
export CXX=$MPICXX
% optimization? -O3 -xHost -ip
export RUNSERIAL="srun -Q -n 1"
export  RUNPARALLEL="srun -Q -n 6"
 ./configure --with-zlib=${ZLIB_DIR} --with-szlib=${SZIP_DIR} --with-pthread
--enable-unsupported --enable-shared --enable-production=yes
--enable-parallel=yes --enable-largefile=yes --with-default-api-version=v18
--prefix=${HOME}/dev/hdf5-1.8.16    --with-pic (?)

gmake
gmake check
gmake install

add HDF5 lib path to LD_LIBRARY_PATH

*NOTE*: make check fails for testcheck_version.sh (why?). 12 FAILED.

## h5py

***NOTE*** NOT NECESSARY ANYMORE, H5PY IS MADE AVAILABLE ON THE CLUSTER FOR PYTHON/2.7.10

get h5py from github -> fix bug with mpi4py
(https://github.com/h5py/h5py/commit/364a77403199087168786234554f459e7d985063)

export CC=mpiicc
python setup.py configure --mpi --hdf5=${HOME}/dev/hdf5-1.8.16/
python setup.py build
python setup.py install --user

* *NOTE*: cannot create h5py.Files in parallel (driver='mpio') in the LUSTRE FS
on the cluster, write to /tmp/ (big data) or /dev/shm (>RAM, small) temporarily
instead

## VTK 5.10
Configure with CMAKE. Set options.

patch Rendering/vtkOpenGL.h:
  #define GL_GLEXT_LEGACY
+#define GLX_GLEXT_LEGACY
 #if defined(__APPLE__) && (defined(VTK_USE_CARBON) || defined(VTK_USE_COCOA))

patch Rendering/vtkXOpenGLRenderWindow.cxx:
-//#define GLX_GLXEXT_LEGACY
+#define GLX_GLXEXT_LEGACY
 #include "GL/glx.h"

mkdir build
cd build
ccmake ../VTK5\*
`c, t`
```
    - -D BUILD_EXAMPLES:BOOL=OFF
    - -D BUILD_SHARED_LIBS:BOOL=ON
    - -D BUILD_TESTING:BOOL=OFF
    - -D CMAKE_INSTALL_PREFIX...
    - -D PYTHON_EXECUTABLE:FILEPATH="${PYTHON}"
    - -D VTK_PYTHON_SETUP_ARGS:STRING="--prefix="${CMAKE_INSTALL_PREFIX}"
--root=. --single-version-externally-managed"
    - -D VTK_USE_GL2PS:BOOL=ON
    - -D VTK_USE_SYSTEM_TIFF:BOOL=ON
    - -D VTK_WRAP_PYTHON:BOOL=ON
    - -D VTK_WRAP_TCL:BOOL=OFF
    etc
```

make
make install
(several times if needed)

add paths:
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/dev/vtk-5.10.1/bin
export PYTHONPATH=${PYTHONPATH}:${HOME}/dev/vtk-5.10.1/bin
export PYTHONPATH=${PYTHONPATH}:${HOME}/dev/vtk-5.10.1/Wrapping/Python

## PCRE (SWIG dep)
export CC=icc
export CXX=icpc
export CFLAGS='-O2 -xHost -ip'
export CXXFLAGS='-O2 -xHost -ip'
(guessed)

configure with options from hashstack/yaml:
./configure --disable-dependency-tracking --enable-utf8 --enable-pcre8 --enable-pcre16 --enable-pcre32 --enable-unicode-properties --enable-jit --prefix=${HOME}/dev/pcre-8.38 

make
make install

## SWIG
./configure --with-pcre-prefix=${PCRE_DIR} --without-octave --without-java --with-boost=/home/apps/lib/boost/1.57 --prefix=${HOME}/dev/swig-3.0.8
    % --with-swiglibdir=${HOME}/dev/swig-3.0.8/share/swig-3.0.8

## EIGEN

## UFL
python setupy.py install --user

## FIAT
python setupy.py install --user

## INSTANT
python setupy.py install --user

## FFC
add SWIG to head of PATH, else finds old system swig executable:
export PATH=${SWIG_DIR}/bin:${PATH}
export CXX=icpc
export CXXFLAGS='-O3 -xHost -ip'   # (??)
python setup.py install --prefix=${HOME}/.local

(--user prefix bug, see https://bitbucket.org/fenics-project/ffc/issues/81/passing-user-instead-of-prefix-to-setuppy)

## UFLACS
python setupy.py install --user

## dolfin
configure on cluster -- srun -I -n 20 --pty /bin/bash

module load python/2.7.10 boost/1.59 hdf5/1.8.15 impi/5.1.1.109 intel/14.0.2
set env variables (compilers) from cluster_env.sh
set compiler flags
export CFLAGS='-O3 -xHost -ip -DNDEBUG -mkl'   # ??
export FCFLAGS='-O3 -xHost -ip -DNDEBUG -mkl'
export CXXFLAGS='-O3 -xHost -ip -DNDEBUG -mkl'

in dev/build/dolfin:
mkdir build_dir; cd build_dir

~/dev/cmake/bin/cmake .. -DBOOST_ROOT=/home/apps/lib/boost/1.59 \
      -DCMAKE_INSTALL_PREFIX=/home/dnolte/dev/dolfin \
      -DCMAKE_BUILD_TYPE="" \
      -DCMAKE_CXX_COMPILER=mpiicpc \
      -DCMAKE_CXX_FLAGS="-O3 -xHost -ip -DNDEBUG -mkl" \
      -DCMAKE_C_COMPILER=mpiicc \
      -DCMAKE_C_FLAGS="-O3 -xHost -ip -DNDEBUG -mkl" \
      -DCMAKE_Fortran_COMPILER=mpiif90 \
      -DCMAKE_Fortran_FLAGS="-O3 -xHost -ip -DNDEBUG -mkl" \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DHDF5_DIR=/home/apps/lib/hdf5/1.8.15 \
      -DHDF5_LIBRARIES=/home/apps/lib/hdf5/1.8.15/lib/libphdf5.so \
      -DHDF5_C_COMPILER_EXECUTABLE=/home/apps/lib/hdf5/1.8.15/bin/h5pcc \
      -DDOLFIN_ENABLE_UMFPACK=ON \
      -DDOLFIN_ENABLE_CHOLMOD=ON \
      -DUMFPACK_DIR=/home/dnolte/dev/petsc \
      -DUMFPACK_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DUMFPACK_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DCHOLMOD_DIR=/home/dnolte/dev/petsc \
      -DCHOLDMOD_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DCHOLDMOD_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DAMD_DIR=/home/dnolte/dev/petsc \
      -DAMD_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DAMD_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DCAMD_DIR=/home/dnolte/dev/petsc \
      -DCAMD_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DCAMD_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DCOLAMD_DIR=/home/dnolte/dev/petsc \
      -DCOLAMD_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DCOLAMD_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DCCOLAMD_DIR=/home/dnolte/dev/petsc \
      -DCCOLAMD_LIBRARY=/home/dnolte/dev/petsc/lib \
      -DCCOLAMD_INCLUDE_DIR=/home/dnolte/dev/petsc/include \
      -DPARMETIS_INCLUDE_DIRS=/home/dnolte/dev/petsc/include \
      -DPARMETIS_LIBRARY=/home/dnolte/dev/petsc/lib/libparmetis.so \
      -DMETIS_LIBRARY=/home/dnolte/dev/petsc/lib/libmetis.so \
      -DPETSC_DIR=/home/dnolte/dev/petsc \
      -DSCOTCH_INCLUDE_DIRS=/home/dnolte/dev/petsc/include \
      -DSCOTCH_LIBRARY=/home/dnolte/dev/petsc/lib/libscotch.a \
      -DSCOTCH_DIR=/home/dnolte/dev/petsc \
      -DSLEPC_DIR=/home/dnolte/dev/slepc \
      -DSLEPC_INCLUDE_DIRS=/home/dnolte/dev/slepc/include \
      -DSLEPC_LIBRARY=/home/dnolte/dev/slepc/lib/libslepc.so \
      -DZLIB_LIBRARY=/usr/lib64/libz.so \
      -DEIGEN3_INCLUDE_DIR=/home/dnolte/dev/eigen/include/eigen3 \
      -DMPIEXEC_MAX_NUMPROCS=120 \
      -DDOLFIN_ENABLE_PASTIX=OFF \
      -DDOLFIN_ENABLE_TRILINOS=OFF \
      -DDOLFIN_ENABLE_BENCHMARKS=ON \
      -DVTK_DIR=/home/dnolte/dev/vtk-5.10.1 \
2>&1 | tee cmake.log
    

***NOTE: This configures dolfin with PETSc external packages instead of using system's optimized suitesparse and parmetis. Maybe it's better to have one consistent version; and PETSc was compiled with -O3 -xCORE-AVX-I flags and with MKL.  ***


