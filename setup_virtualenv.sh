#!/bin/bash
source env_build.sh $ARCH

# create new virtualenv without prompt in location: 
#       $HOME/.local/share/virtualenvs/
# export WORKON_HOME=${PREFIX}/share/virtualenvs/
pew new -d fenics-${TAG} -i ply -i flufl.lock
pew in fenics-${TAG} pew add ${EBROOTPYTHON}/lib/python2.7/site-packages

# install system modules into virtualenv
# pymod="/home/apps/python/2.7.10/lib/python2.7/site-packages"
# pylink="${PREFIX}/lib/python2.7-sysmodules"
# mkdir -p $pylink
# modules=( numpy numpy-1.10.1-py2.7.egg-info \
#     mpi4py mpi4py-2.0.0-py2.7.egg-info \
#     h5py-2.5.0-py2.7-linux-x86_64.egg )

# for mod in "${modules[@]}"; 
# do
#     echo "symlinking ${pymod}/${mod} to $pylink"
#     ln -s ${pymod}/${mod} $pylink
# done

# pew in fenics-${TAG} pew add ${pylink}


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
